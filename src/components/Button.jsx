import React from 'react';
import styled from '@datapunt/asc-core';
import { themeColor } from '@datapunt/asc-ui';

const StyledInput = styled.input`
	display: none;
	&:checked+label {
		color: #fff;
		background-color: #ff9100;
		border-color: black;
	}
`

const StyledLabel = styled.label`
	display: block;
	width: 176px;
	height: 44px;
	padding: 11px 8px;
	margin-top: 2px;
	margin-bottom: 2px;
	border: 1px solid ${themeColor('tint', 'level3')};
	cursor: pointer;

	&:hover {
		border-color: black;
	}
`

const StyledText = styled.span`
	font-weight: bold;
	font-size: 18px;
`

const Button = ({ id, value, col, row, handleChange }) => (
	<>
		<StyledInput
			type="radio"
			name="buttons"
			id={id}
			value={value}
			data-col={col}
			data-row={row}
			onChange={handleChange}
		/>
		<StyledLabel htmlFor={id}>
			<StyledText>{value}</StyledText>
		</StyledLabel>
	</>
);

export default Button;