import React, { useRef, useState, useEffect } from 'react';
import {
	GlobalStyle,
	ThemeProvider
} from '@datapunt/asc-ui';
import styled from '@datapunt/asc-core';
import * as d3 from 'd3';
import Button from './components/Button';

const opleidingen = [
	{ name: 'Praktijkonderwijs', value: 3, col: 1, row: 1},
	{ name: 'VMBO - B', value: 5, col: 1, row: 2},
	{ name: 'VMBO - B/K', value: 12, col: 1, row: 3},
	{ name: 'VMBO - K', value: 10, col: 1, row: 4},
	{ name: 'VMBO - G/T', value: 15, col: 1, row: 5},
	{ name: 'VMBO - T/HAVO', value: 15, col: 1, row: 6},
	{ name: 'HAVO', value: 10, col: 1, row: 7},
	{ name: 'HAVO/VWO', value: 15, col: 1, row: 8},
	{ name: 'VWO', value: 15, col: 1, row: 9}
]

const opleidingen2 = [
	{ name: 'Praktijkonderwijs', value: 3, col: 2, row: 1},
	{ name: 'VMBO - B', value: 5, col: 2, row: 2},
	{ name: 'VMBO - B/K', value: 12, col: 2, row: 3},
	{ name: 'VMBO - K', value: 10, col: 2, row: 4},
	{ name: 'VMBO - G/T', value: 15, col: 2, row: 5},
	{ name: 'VMBO - T/HAVO', value: 15, col: 2, row: 6},
	{ name: 'HAVO', value: 10, col: 2, row: 7},
	{ name: 'HAVO/VWO', value: 15, col: 2, row: 8},
	{ name: 'VWO', value: 15, col: 2, row: 9}
]

const opleidingen3 = [
	{ name: 'Praktijkonderwijs', value: 3, col: 3, row: 1},
	{ name: 'VMBO - B', value: 5, col: 3, row: 2},
	{ name: 'VMBO - B/K', value: 12, col: 3, row: 3},
	{ name: 'VMBO - K', value: 10, col: 3, row: 4},
	{ name: 'VMBO - G/T', value: 15, col: 3, row: 5},
	{ name: 'VMBO - T/HAVO', value: 15, col: 3, row: 6},
	{ name: 'HAVO', value: 10, col: 3, row: 7},
	{ name: 'HAVO/VWO', value: 15, col: 3, row: 8},
	{ name: 'VWO', value: 15, col: 3, row: 9}
]

const StyledContainer = styled.div`
	width: 100%;
`

const StyledButtonColumn = styled.div`
	display: inline-block;
	vertical-align: top;
`

const StyledVisContainer = styled.div`
	display: inline-block;
	width: calc((100% - (176px * 3)) / 2);
	height: 416px;
`
function App() {
	const svgLeft = useRef();
	const svgRight = useRef();

	const svgColl = {
		left: svgLeft,
		right: svgRight
	}

	const [row, setRow] = useState(1);
	const [column, setColumn] = useState(1);
	const [selectedData, setSelectedData] = useState(opleidingen);

	const handleChange = (evt) => {
		setRow(parseInt(evt.target.dataset.row));
		setColumn(parseInt(evt.target.dataset.col));
	}

	useEffect(() => {
		Object.entries(svgColl).forEach(([ position, ref ])=>{
			if (ref.current) {
				// select svg
				const svg = d3.select(ref.current);

				// clear current svg
				svg.selectAll('*').remove();

				if (position === 'left' ? column !== 3 : column !== 1) {
					// function to get height and width of container
					let width;
					let height;

					function getHeightWidth () {
						const bbox = svg.node().getBoundingClientRect()
						width = bbox.width
						height = bbox.height
					};

					let lineData;

					function setCoordinates (index) {
						if ((position === 'left' && column === 1) || (position === 'right' && column === 2)) {
							lineData = [
								{ "x": 8, "y": (height * (( 1 / selectedData.length ) * ( row ))) - (42 - (( 44/selectedData.length ) * index ))},
								{ "x": width * (1/3), "y": (height * (( 1 / selectedData.length ) * ( row ))) - (42 - (( 44/selectedData.length ) * index )) },
								{ "x": width * (2/3), "y": (height * (( 1 / selectedData.length ) * ( index + 1 ))) - 10 },
								{ "x": width - 8, "y": (height * (( 1 / selectedData.length ) * ( index + 1 ))) - 10 }
							];
						} else if ((position === 'left' && column === 2) || (position === 'right' && column === 3)) {
							lineData = [
								{ "x": width - 8, "y": (height * (( 1 / selectedData.length ) * ( row ))) - (42 - (( 44/selectedData.length ) * index )) },
								{ "x": width * (2/3), "y": (height * (( 1 / selectedData.length ) * ( row ))) - (42 - (( 44/selectedData.length ) * index )) },
								{ "x": width * (1/3), "y": (height * (( 1 / selectedData.length ) * ( index + 1 ))) - 10 },
								{ "x": 8, "y": (height * (( 1 / selectedData.length ) * ( index + 1 ))) - 10 }
							];
						}
					}

					const lineFunction = d3.line()
 						.x(function(d) { return d.x; })
						.y(function(d) { return d.y; });

					selectedData.forEach((element, index) => {
						// create line
						const line = svg.append("path")
							.attr('stroke-width', 2)
							.attr('stroke', '#ff9100')
							.attr('fill', 'transparent');

						// onload
						getHeightWidth();
						setCoordinates(index);
						line.attr("d", lineFunction(lineData));

						// onresize
						window.addEventListener('resize', function () {
							getHeightWidth();
							setCoordinates(index);
							line.attr("d", lineFunction(lineData));
						}); 
					})
				}
			}
		})
	}, [svgColl, selectedData, row, column])

	return (
		<ThemeProvider>
			<GlobalStyle />
			<StyledContainer>
				<StyledButtonColumn>
					{opleidingen.map(entry =>
						<Button
							key={entry.col + '_' + entry.row}
							id={entry.col + '_' + entry.row}
							value={entry.name}
							col={entry.col}
							row={entry.row}
							handleChange={handleChange}
						/>
					)}
				</StyledButtonColumn>
				<StyledVisContainer >
					<svg width='100%' height='100%' ref={svgLeft} />
				</StyledVisContainer>
				<StyledButtonColumn>
					{opleidingen2.map(entry =>
						<Button
							key={entry.col + '_' + entry.row}
							id={entry.col + '_' + entry.row}
							value={entry.name}
							col={entry.col}
							row={entry.row}
							handleChange={handleChange}
						/>
					)}
				</StyledButtonColumn>
				<StyledVisContainer>
					<svg width='100%' height='100%' ref={svgRight} />
				</StyledVisContainer>
				<StyledButtonColumn>
					{opleidingen3.map(entry =>
						<Button
							key={entry.col + '_' + entry.row}
							id={entry.col + '_' + entry.row}
							value={entry.name}
							col={entry.col}
							row={entry.row}
							handleChange={handleChange}
						/>
					)}
				</StyledButtonColumn>
			</StyledContainer>
		</ThemeProvider>
	);
}

export default App;